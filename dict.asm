section .data


section .text

extern string_equals

global find_word


; rdi - key pointer
; rsi - start of dict pointer
find_word:
  .loop:
    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 1
    je .found
    cmp qword[rsi], 0
    je .not_found
    mov rsi, [rsi]
    jmp .loop
  .found:
    mov rax, rsi
    ret
  .not_found:
    xor rax, rax
    ret








